package homework;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {
    public static void main(String[] args) {

        System.out.println("\n------Task#1------\n");

        String str = "    Java is fun      ";

        System.out.println(countWords(str));

        System.out.println("\n------Task#2------\n");

        str = "TechGlobal is a QA bootcamp";

        System.out.println(countA(str));

        System.out.println("\n------Task#3------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123));

        System.out.println(countPos(list));

        System.out.println("\n------Task#4------\n");

        list = new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60));

        System.out.println(removeDuplicateNumbers(list));

        System.out.println("\n------Task#5------\n");

        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"));

        System.out.println(removeDuplicateElements(list1));


        System.out.println("\n------Task#6------\n");

        str = "   I   am      learning     Java      ";

        System.out.println(removeExtraSpaces(str));


        System.out.println("\n------Task#7------\n");

        int[] arr1 = {3, 0, 0, 7, 5, 10};

        int[] arr2 = {6, 3, 2};

        System.out.println(Arrays.toString(add(arr1, arr2)));

        System.out.println("\n------Task#8------\n");

        int[] numbers = {10, -13, 5, 70, 15, 57};

        System.out.println(Arrays.toString(findClosestTo10(numbers)));

    }


    //Task#1
    public static int countWords(String str) {
        if (str == null || str.isEmpty()) {
            return 0;
        }
        String[] words = str.trim().split("\\s");
        return words.length;
    }//Task#1 End

    //Task#2
    public static int countA(String str) {

        int countA = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.toLowerCase().charAt(i) == 'a')
                countA++;
        }
        return countA;
    }//Task#2 End

    //Task#3
    public static int countPos(ArrayList<Integer> list) {

        int countPositive = 0;

        for (Integer i : list) {
            if (i > 0)
                countPositive++;
        }
        return countPositive;
    }//Tash#3 End

    //Task#4
    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> numbers) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < numbers.size(); i++) {
            int currentNumber = numbers.get(i);
            boolean isDuplicate = false;
            for (int j = i + 1; j < numbers.size(); j++) {
                if (currentNumber == numbers.get(j)) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                result.add(currentNumber);
            }
        }
        return result;
    }//Task#4 End

    //Task#5
    public static ArrayList<String> removeDuplicateElements(ArrayList<String> list) {
        ArrayList<String> result = new ArrayList<>();
        for (String element : list) {
            if (!result.contains(element)) {
                result.add(element);
            }
        }
        return result;
    }//Task#5 End

    //Task#6
    public static String removeExtraSpaces(String str) {
        String result = str.replaceAll("\\s+", " ");

        result = result.trim();

        return result;
    }//Task#6 End

    //Task#7
    public static int[] add(int[] arr1, int[] arr2) {
        int length = Math.max(arr1.length, arr2.length);
        int[] result = new int[length];
        for (int i = 0; i < length; i++) {
            int sum = 0;
            if (i < arr1.length) {
                sum += arr1[i];
            }
            if (i < arr2.length) {
                sum += arr2[i];
            }
            result[i] = sum;
        }
        return result;
    }//Task#7 End

    //Task#8
    public static int[] findClosestTo10(int[] arr) {
        int closest = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == 10) {
                continue;
            }
            if (Math.abs(arr[i] - 10) < Math.abs(closest - 10)) {
                closest = arr[i];
            } else if (Math.abs(arr[i] - 10) == Math.abs(closest - 10)) {
                closest = Math.min(closest, arr[i]);
            }
        }
        return new int[]{closest};
    }//Task#8 End
}
