package homework;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework09 {
    public static void main(String[] args) {

        System.out.println("\n------Task#1------\n");

        int[] numbers = {3, 4, 3, 3, 5, 5, 6, 6, 7};
        System.out.println(firstDuplicatedNumber(numbers));

        System.out.println("\n------Task#2------\n");

        String[] words = {"Z", "abc", "z", "123", "#"};
        System.out.println(firstDuplicatedString(words));

        System.out.println("\n------Task#3------\n");

        numbers = new int[]{1, 2, 5, 0, 7};

        System.out.println(duplicatedNumbers(numbers));

        System.out.println("\n------Task#4------\n");

        words = new String[]{"A", "foo", "12", "Foo", "bar", "a", "a", "java"};

        System.out.println(duplicatedStrings(words));

        System.out.println("\n------Task#5------\n");

        String[] words1 = {"abc", "foo", "bar"};

        reversedArray(words1);

        System.out.println("\n------Task#6------\n");

        String str = "Java is fun";

        System.out.println(reverseStringWords(str));

    }

    //Task#1
    public static int firstDuplicatedNumber(int[] num) {
        int n = num.length;

        for (int i = 0; i < n; i++) {
            int num1 = Math.abs(num[i]);
            if (num[num1 - 1] > 0) {
                num[num1 - 1] = -num[num1 - 1];
            } else {
                return num1;
            }
        }

        return -1;
    } //Task#1 End

    //Task#2
    public static String firstDuplicatedString(String[] str) {
        int n = str.length;

        for (int i = 0; i < n; i++) {
            String str1 = str[i].toLowerCase();
            for (int j = i + 1; j < n; j++) {
                if (str1.equals(str[j].toLowerCase())) {
                    return str1;
                }
            }
        }

        return "There is no duplicates";
    }//Task#2 End

    //Task#3

    public static ArrayList<Integer> duplicatedNumbers(int[] num) {
        int n = num.length;
        ArrayList<Integer> dupes = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            int num1 = Math.abs(num[i]);
            if (num[num1 - 1] > 0) {
                num[num1 - 1] = -num[num1 - 1];
            } else {
                dupes.add(num1);
            }
        }

        return dupes;
    }//Task#3 End

    //Task#4
    public static ArrayList<String> duplicatedStrings(String[] str) {
        int n = str.length;
        ArrayList<String> dupes = new ArrayList<String>();

        for (int i = 0; i < n; i++) {
            String str1 = str[i].toLowerCase();
            for (int j = i + 1; j < n; j++) {
                if (str[j].toLowerCase().equals(str1)) {
                    if (!dupes.contains(str1)) {
                        dupes.add(str1);
                    }
                }
            }
        }

        return dupes;
    }//Task#4 End

    //Task#5
    public static void reversedArray(String[] str) {
        int n = str.length;
        int mid = n / 2;

        for (int i = 0; i < mid; i++) {
            String temp = str[i];
            str[i] = str[n - i - 1];
            str[n - i - 1] = temp;
        }

        System.out.println(Arrays.toString(str));
    }//Task#5 End


    //Task#6

    public static String reverseStringWords(String str) {
        String[] words = str.split(" ");
        StringBuilder reverse = new StringBuilder();

        for (String word : words) {
            StringBuilder sb = new StringBuilder(word);
            sb.reverse();
            reverse.append(sb.toString() + " ");
        }

        return reverse.toString().trim();
    }//Task#6 End
}
