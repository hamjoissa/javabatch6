package homework;

import java.util.Arrays;
import java.util.Date;

public class Homework11 {
    public static void main(String[] args) {
        System.out.println("\n------Task#1------\n");

        String str = "   Hello   ";

        System.out.println(noSpace(str));

        System.out.println("\n------Task#2------\n");

        str = "Hello";

        System.out.println(replaceFirstLast(str));

        System.out.println("\n------Task#3------\n");

        System.out.println(hasVowel(str));

        System.out.println("\n------Task#4------\n");

        int age = 2000;

        checkAge(age);

        System.out.println("\n------Task#5------\n");

        int n1 = 3,n2 = 4, n3 = 5;

        System.out.println(averageOfEdges(n1, n2, n3));

        System.out.println("\n------Task#6------\n");

        String[] words = {"appium", "123", "ABC", "java"};

        System.out.println(Arrays.toString(noA(words)));

        System.out.println("\n------Task#7------\n");

        int[] number = {3, 4, 5, 6};

        System.out.println(Arrays.toString(no3or5(number)));

        System.out.println("\n------Task#8------\n");

        int[] numbers = {7, 4, 11, 23, 17};

        System.out.println(countPrimes(numbers));


    }



    //Task#1
    public static String noSpace(String str) {
        return str.replaceAll("\\s", "");
    }//Task#1 End


    //Task#2
    public static String replaceFirstLast(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        char first = str.charAt(0);
        char last = str.charAt(str.length() - 1);
        String middle = str.substring(1, str.length() - 1);
        return last + middle + first;
    }//Task#2 End


    // Task#3
    public static boolean hasVowel(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        String vowels = "aeiouAEIOU";
        for (int i = 0; i < str.length(); i++) {
            if (vowels.indexOf(str.charAt(i)) != -1) {
                return true;
            }
        }
        return false;
    }// Task#3 End


    // Task#4
    public static void checkAge(int yearOfBirth) {
        Date currentDate = new Date();
        int currentYear = currentDate.getYear() + 1900;
        int age = currentYear - yearOfBirth;

        if (age < 0 || age > 100) {
            System.out.println("AGE IS NOT VALID");
        } else if (age < 16) {
            System.out.println("AGE IS NOT ALLOWED");
        } else {
            System.out.println("AGE IS ALLOWED");
        }
    }//Task#4 End


    // Task#5
    public static int averageOfEdges(int num1, int num2, int num3) {
        int max = Math.max(num1, Math.max(num2, num3));
        int min = Math.min(num1, Math.min(num2, num3));
        return (max + min) / 2;
    }//Task#5 End


    // Task#6
    public static String[] noA(String[] arr) {
        String[] newArr = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].startsWith("A") || arr[i].startsWith("a")) {
                newArr[i] = "###";
            } else {
                newArr[i] = arr[i];
            }
        }
        return newArr;
    }//Task#6 End


    // Task#7
    public static int[] no3or5(int[] arr) {
        int[] newArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 3 == 0 && arr[i] % 5 == 0) {
                newArr[i] = 101;
            } else if (arr[i] % 5 == 0) {
                newArr[i] = 99;
            } else if (arr[i] % 3 == 0) {
                newArr[i] = 100;
            } else {
                newArr[i] = arr[i];
            }
        }
        return newArr;
    }//Task#7 End


    // Task#8
    public static int countPrimes(int[] arr) {
        int count = 0;
        for (int num : arr) {
            if (num > 1 && isPrime(num)) {
                count++;
            }
        }
        return count;
    }

    public static boolean isPrime(int num) {
        if (num == 2) {
            return true;
        } else if (num <= 1 || num % 2 == 0) {
            return false;
        }
        for (int i = 3; i <= Math.sqrt(num); i += 2) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }//Task#8 End
}
