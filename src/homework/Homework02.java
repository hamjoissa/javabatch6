package homework;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n-------------------------TASK#1--------------------\n");

        System.out.println("Please enter your first number");
        int num1 = input.nextInt();

        System.out.println("The number 1 entered by user is = " + num1);

        System.out.println("Please enter your second number");

        int num2 = input.nextInt();

        System.out.println("The number 2 entered by user is = " + num2);

        int sum = num1 + num2;

        System.out.println("The sum of number 1 and 2 entered by user is = " + sum);

        System.out.println("\n-------------------------TASK#2--------------------\n");

        System.out.println("Please enter your 2 number");
        num1 = input.nextInt();
        num2 = input.nextInt();


        System.out.println("The product of the given 2 numbers is: " + (num1 * num2));

        System.out.println("\n-------------------------TASK#3--------------------\n");

        System.out.println("Please enter your 2 number");
        double number = input.nextDouble();
        double number2 = input.nextDouble();

        double result = number + number2, result2 = number * number2, result3 = number - number2,
                result4 = number / number2, result5 = number % number2;

        System.out.println("The sum of the given number is = " + result);
        System.out.println("The product of the given number is = " + result2);
        System.out.println("The subtraction of the given number is = " + result3);
        System.out.println("The division of the given number is = " + result4);
        System.out.println("The remainder of the given number is = " + result5);

        System.out.println("\n-------------------------TASK#4--------------------\n");


        System.out.println(-10 + 7 * 5); // Question #1
        System.out.println((72 + 24) % 24); // Question #2
        System.out.println(10 + -3 * 9 / 9); // Question #3
        System.out.println(5 + 18 / 3 * 3 - (6 % 3)); // Question #4


        System.out.println("\n-------------------------TASK#5--------------------\n");

        System.out.println("Please enter your 2 number");
        int number3 = input.nextInt();
        int number4 = input.nextInt();

        int sum3 = (number3 + number4) / 2;

        System.out.println("The average of the given numbers is: " + sum3);

        System.out.println("\n-------------------------TASK#6--------------------\n");


        System.out.println("Please enter your 5 number");
        int pick1 = input.nextInt();
        int pick2 = input.nextInt();
        int pick3 = input.nextInt();
        int pick4 = input.nextInt();
        int pick5 = input.nextInt();


        int total = (pick1 + pick2 + pick3 + pick4 + pick5) / 5;

        System.out.println("The average of the given numbers is: " + total);

        System.out.println("\n-------------------------TASK#7--------------------\n");


        System.out.println("Please enter your 3 number");
        int i1 = input.nextInt();
        int i2 = input.nextInt();
        int i3 = input.nextInt();

        System.out.println("The " + i1 + " multiplied with " + i1 + " is = " + (i1 * i1));

        System.out.println("The " + i2 + " multiplied with " + i2 + " is = " + (i2 * i2));

        System.out.println("The " + i3 + " multiplied with " + i3 + " is = " + (i3 * i3));

        System.out.println("\n-------------------------TASK#8--------------------\n");

        System.out.println("Please enter the length of the square");
        int side = input.nextInt();

        System.out.println("Perimeter of the square = " + (side * side));

        System.out.println("Area of the square = " + (4 * side));

        System.out.println("\n-------------------------TASK#9--------------------\n");


        double salary = 3 * 90000;

        System.out.println("A software engineer in test can earn " + salary + " in 3 years.");

        System.out.println("\n-------------------------TASK#10--------------------\n");

        System.out.println("Please enter your favorite book");
        String favBook = input.nextLine();

        System.out.println("Please enter your favorite color");
        String favColor = input.nextLine();

        System.out.println("Please enter your favorite number");
        int favNumber = input.nextInt();

        System.out.println("User's favorite book is: " + favBook + "\nUser's favorite color is: " + favColor +
                "\nUser's favorite number is: " + favNumber);




        System.out.println("\n-------------------------TASK#11--------------------\n");

        System.out.println("Please enter your first name");
        String firstName = input.next();


        System.out.println("Please enter your last name");
        String lastName = input.next();

        System.out.println("Please enter your age");
        int age = input.nextInt();

        System.out.println("Please enter your email address");
        String emailAddress = input.next();
        input.nextLine();

        System.out.println("Please enter your phone number");
        String phoneNumber = input.nextLine();

        System.out.println("Please enter your address");
        String address = input.nextLine();

        System.out.println("\tUser who joined this program is " + firstName + " " + lastName + ". " + firstName +
                "'s age is " + age + ". " + firstName + "'s email\naddress is " + emailAddress + ", phone number is "
                + phoneNumber + ", and address\nis " + address + ".");


    }
}
