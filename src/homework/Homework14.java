package homework;

public class Homework14 {
    public static void main(String[] args) {
        System.out.println("\n------Task#1------\n");

        int num = 18;

        System.out.println(fizzBuzz1(num));

        System.out.println("\n------Task#2------\n");

        num = 15;

        System.out.println(fizzBuzz2(num));


        System.out.println("\n------Task#3------\n");

        String word = "ab110c045d";

        System.out.println(findSumNumbers(word));


        System.out.println("\n------Task#4------\n");

        System.out.println(findBiggestNumber(word));


        System.out.println("\n------Task#5------\n");

        word = "abbcca";

        System.out.println(countSequenceOfCharacters(word));


    }

    //Task#1

    public static int fizzBuzz1(int num) {
        for (int i = 1; i <= num; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
        return num;
    }//Task#1 End


    //Task#2

    public static String fizzBuzz2(int num) {
        if (num % 3 == 0 && num % 5 == 0) {
            return "FizzBuzz";
        } else if (num % 3 == 0) {
            return "Fizz";
        } else if (num % 5 == 0) {
            return "Buzz";
        } else {
            return String.valueOf(num);
        }
    }//Task#2 End


    //Task#3

    public static int findSumNumbers(String input) {
        int sum = 0;
        StringBuilder currentNumber = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isDigit(c)) {
                currentNumber.append(c);
            } else {
                if (currentNumber.length() > 0) {
                    sum += Integer.parseInt(currentNumber.toString());
                    currentNumber.setLength(0);
                }
            }
        }

        if (currentNumber.length() > 0) {
            sum += Integer.parseInt(currentNumber.toString());
        }

        return sum;
    }//Task#3 End


    //Task#4

    public static int findBiggestNumber(String input) {
        int biggestNumber = Integer.MIN_VALUE;
        StringBuilder currentNumber = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isDigit(c)) {
                currentNumber.append(c);
            } else {
                if (currentNumber.length() > 0) {
                    int number = Integer.parseInt(currentNumber.toString());
                    if (number > biggestNumber) {
                        biggestNumber = number;
                    }
                    currentNumber.setLength(0);
                }
            }
        }

        if (currentNumber.length() > 0) {
            int number = Integer.parseInt(currentNumber.toString());
            if (number > biggestNumber) {
                biggestNumber = number;
            }
        }

        return biggestNumber;
    }//Task#4 End


    //Task#5

    public static String countSequenceOfCharacters(String input) {
        if (input.isEmpty()) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        char currentChar = input.charAt(0);
        int count = 1;

        for (int i = 1; i < input.length(); i++) {
            char c = input.charAt(i);

            if (c == currentChar) {
                count++;
            } else {
                result.append(currentChar).append(count);
                currentChar = c;
                count = 1;
            }
        }

        result.append(currentChar).append(count);

        return result.toString();
    }//Task#5 End
}
