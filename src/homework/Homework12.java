package homework;

import java.util.Arrays;

public class Homework12 {
    public static void main(String[] args) {

        System.out.println("\n------Task#1------\n");

        String str = "123Hello";

        System.out.println(noDigit(str));

        System.out.println("\n------Task#2------\n");

        str = "JAVA";

        System.out.println(noVowel(str));

        System.out.println("\n------Task#3------\n");

        str = "John's age is 29";

        System.out.println(sumOfDigits(str));

        System.out.println("\n------Task#4------\n");

        str = "java";

        System.out.println(hasUpperCase(str));

        System.out.println("\n------Task#5------\n");

        int n1 = 5, n2 = 5, n3 = 8;

        System.out.println(middleInt(n1, n2, n3));

        System.out.println("\n------Task#6------\n");

        int[] numbers = {1, 2, 3, 4};

        System.out.println(Arrays.toString(no13(numbers)));

        System.out.println("\n------Task#7------\n");


        System.out.println(Arrays.toString(arrFactorial(numbers)));

        System.out.println("\n------Task#8------\n");

        str = "iusf7623459%$@#!";

        System.out.println(Arrays.toString(categorizeCharacters(str)));
    }

    //Task#1

    public static String noDigit(String input) {
        StringBuilder result = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (!Character.isDigit(c)) {
                result.append(c);
            }
        }
        return result.toString();
    }//Task#1 End


    //Task#2

    public static String noVowel(String input) {
        String vowels = "aeiouAEIOU";
        StringBuilder result = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (vowels.indexOf(c) == -1) {
                result.append(c);
            }
        }
        return result.toString();
    }//Task#2 End


    //Task#3

    public static int sumOfDigits(String input) {
        int sum = 0;
        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                sum += digit;
            }
        }
        return sum;
    }//Task#3 End


    //Task#4

    public static boolean hasUpperCase(String input) {
        for (char c : input.toCharArray()) {
            if (Character.isUpperCase(c)) {
                return true;
            }
        }
        return false;
    }//Task#4 End


    //Task#5

    public static int middleInt(int a, int b, int c) {
        if ((a >= b && a <= c) || (a <= b && a >= c)) {
            return a;
        } else if ((b >= a && b <= c) || (b <= a && b >= c)) {
            return b;
        } else {
            return c;
        }
    }//Task#5 End


    //Task#6

    public static int[] no13(int[] array) {
        int[] modifiedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 13) {
                modifiedArray[i] = 0;
            } else {
                modifiedArray[i] = array[i];
            }
        }
        return modifiedArray;
    }//Task#6 End


    //Task#7

    public static int[] arrFactorial(int[] array) {
        int[] modifiedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            modifiedArray[i] = factorial(array[i]);
        }
        return modifiedArray;
    }

    public static int factorial(int num) {
        if (num == 0 || num == 1) {
            return 1;
        } else {
            return num * factorial(num - 1);
        }
    }//Task#7 End


    //Task#8

    public static String[] categorizeCharacters(String input) {
        String[] categorized = new String[3];
        categorized[0] = "";
        categorized[1] = "";
        categorized[2] = "";

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isLetter(c)) {
                categorized[0] += c;
            } else if (Character.isDigit(c)) {
                categorized[1] += c;
            } else if (!Character.isWhitespace(c)) {
                categorized[2] += c;
            }
        }

        return categorized;
    }//Task#8 End
}


    

