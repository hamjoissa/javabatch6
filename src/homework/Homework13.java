package homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("\n------Task#1------\n");

        String str = "hello";

        System.out.println(hasLowerCase(str));

        System.out.println("\n------Task#2------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 1, 10));

        System.out.println(noZero(list));

        System.out.println("\n------Task#3------\n");

        int[] numbers = {1, 2, 3};

        System.out.println(Arrays.toString(numberAndSquare(numbers)));

        System.out.println("\n------Task#4------\n");

        String[] words = {"abc", "foo", "java"};

        String word = "hello";

        System.out.println(containsValue(words, word));

        System.out.println("\n------Task#5------\n");

        str = "Java is fun";

        System.out.println(reverseSentence(str));

        System.out.println("\n------Task#6------\n");

        str = "Selenium 123#$%Cypress";

        System.out.println(removeStringSpecialsDigits(str));

        System.out.println("\n------Task#7------\n");

        String[] element = {"123Java", "#$%is", "fun"};

        System.out.println(Arrays.toString(removeArraySpecialsDigits(element)));

        System.out.println("\n------Task#8------\n");

        ArrayList<String> words1 = new ArrayList<>(Arrays.asList("java", "is", "fun"));

        ArrayList<String> words2 = new ArrayList<>(Arrays.asList("abc", "xyz", "123"));

        System.out.println(removeAndReturnCommons(words1, words2));

        System.out.println("\n------Task#9------\n");

        System.out.println(noXInVariables(words2));

    }


    //Task#1

    public static boolean hasLowerCase(String input) {
        for (int i = 0; i < input.length(); i++) {
            if (Character.isLowerCase(input.charAt(i))) {
                return true;
            }
        }

        return false;
    }//Task#1 End


    //Task#2

    public static ArrayList<Integer> noZero(ArrayList<Integer> list) {
        ArrayList<Integer> modifiedList = new ArrayList<>();

        for (Integer num : list) {
            if (num != 0) {
                modifiedList.add(num);
            }
        }

        return modifiedList;
    }//Task#2 End


    //Task#3

    public static int[][] numberAndSquare(int[] array) {
        int[][] result = new int[array.length][2];

        for (int i = 0; i < array.length; i++) {
            result[i][0] = array[i];                   // original number
            result[i][1] = array[i] * array[i];        // square of the number
        }

        return result;
    }//Task#3 End


    //Task#4


    public static boolean containsValue(String[] array, String searchTerm) {
        for (String element : array) {
            if (element.equals(searchTerm)) {
                return true;
            }
        }

        return false;
    }//Task#4 End


    //Task#5

    public static String reverseSentence(String sentence) {
        String[] words = sentence.split(" ");

        if (words.length < 2) {
            return "There is not enough words!";
        }

        StringBuilder reversedSentence = new StringBuilder();

        for (int i = words.length - 1; i >= 0; i--) {
            String word = words[i];
            String reversedWord = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
            reversedSentence.append(reversedWord).append(" ");
        }

        return reversedSentence.toString().trim();
    }//Task#5 End


    //Task#6

    public static String removeStringSpecialsDigits(String input) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isLetter(c) || Character.isWhitespace(c)) {
                result.append(c);
            }
        }

        return result.toString();
    }//Task#6 End


    //Task#7

    public static String[] removeArraySpecialsDigits(String[] array) {
        String[] modifiedArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            String element = array[i];
            StringBuilder result = new StringBuilder();

            for (int j = 0; j < element.length(); j++) {
                char c = element.charAt(j);

                if (Character.isLetter(c) || Character.isWhitespace(c)) {
                    result.append(c);
                }
            }

            modifiedArray[i] = result.toString();
        }

        return modifiedArray;
    }//Task#7 End


    //Task#8

    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2) {
        HashSet<String> set1 = new HashSet<>(list1);
        HashSet<String> set2 = new HashSet<>(list2);

        set1.retainAll(set2);

        return new ArrayList<>(set1);
    }//Task#8 End


    //Task#9

    public static ArrayList<String> noXInVariables(ArrayList<String> list) {
        ArrayList<String> modifiedList = new ArrayList<>();

        for (String element : list) {
            if (!element.equalsIgnoreCase("x") && !element.equalsIgnoreCase("X") && !element.contains("x")) {
                modifiedList.add(element);
            }
        }

        return modifiedList;
    }//Task#9 End

}
