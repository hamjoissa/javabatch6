package homework;


import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n-----------------TASK#1-----------------\n");

        String str = ScannerHelper.getFirstName();

        System.out.println("The length of the name is = " + str.length());
        System.out.println("The first character in the name is = " + str.charAt(0));
        System.out.println("The last character in the name is = " + str.charAt(str.length() - 1));
        System.out.println("The first 3 character in the name are = " + str.substring(0, 3));
        System.out.println("The last 3 character in the name are = " + str.substring(str.length() - 3));

        if (str.startsWith("a") || str.startsWith("A")) System.out.println("You are in the club!");
        else System.out.println("Sorry, you are not in the club");

        System.out.println("\n-----------------TASK#2-----------------\n");

        System.out.println("Please enter your full address");

        String address = input.nextLine();
        address.toLowerCase();

        if (address.contains("Chicago")) System.out.println("You are in the club");
        else if (address.contains("Des plaines")) {
            System.out.println("You are welcome to join to the club");
        } else System.out.println("Sorry, you will never be in the club");


        System.out.println("\n-----------------TASK#3-----------------\n");

        System.out.println("Please enter your favorite country");
        String favCountry = input.nextLine();

        if (favCountry.contains("a") || favCountry.contains("A")) System.out.println("A is there");
        else if (favCountry.contains("i") || favCountry.contains("I")) {
            System.out.println("I is there");
        } else if (favCountry.contains("a") || favCountry.contains("A") &&
                favCountry.contains("i") || favCountry.contains("I"))
        System.out.println("A and I are there");
        else System.out.println("A and i are not there");

        System.out.println("\n-----------------TASK#4-----------------\n");

        String str1 = "  Java is FUN  ";

        System.out.println("The first word in the str is: " + str1.trim().toLowerCase().substring(0, 4));
        System.out.println("The second word in the str is: " + str1.trim().toLowerCase().substring(5, 7));
        System.out.println("The third word in the str is: " + str1.trim().toLowerCase().substring(8));

    }
}





