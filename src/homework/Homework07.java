package homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("\n-----Task-1-----\n");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.println("\n-----Task-2-----\n");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White","Black", "Purple"));

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n-----Task-3-----\n");

        ArrayList<Integer> numberss = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));

        System.out.println(numberss);

        Collections.sort(numberss);

        System.out.println(numberss);

        System.out.println("\n-----Task-4-----\n");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(cities);

        Collections.sort(cities);

        System.out.println(cities);

        System.out.println("\n-----Task-5-----\n");

        ArrayList<String> heroes = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));

        System.out.println(heroes);

        System.out.println(heroes.contains("Wolwerine"));

        System.out.println("\n-----Task-6-----\n");

        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));

        Collections.sort(avengers);

        System.out.println(avengers);

        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));

        System.out.println("\n-----Task-7-----\n");

        ArrayList<String> specialchar = new ArrayList<>(Arrays.asList("A", "x", "$", "%", "9", "*", "+", "F", "G"));

        System.out.println(specialchar);

        for (String s : specialchar) {

            System.out.println(s);
        }

        System.out.println("\n-----Task-8-----\n");

        ArrayList<String> computerparts = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(computerparts);

        Collections.sort(computerparts);
        System.out.println(computerparts);

        int startsWithMCount = 0;
        for (String computerpart : computerparts) {
            if (computerpart.startsWith("M") || computerpart.startsWith("m")) {
                startsWithMCount++;
            }
        }
        System.out.println(startsWithMCount);

        int doesNotHaveAOrECount = 0;
        for (String computerpart : computerparts) {
            if (!computerpart.contains("A") && !computerpart.contains("a") && !computerpart.contains("E") && !computerpart.contains("e")) {
                doesNotHaveAOrECount++;
            }
        }
        System.out.println(doesNotHaveAOrECount);



        System.out.println("\n-----Task-9-----\n");

        ArrayList<String> kitchenObjects = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        System.out.println(kitchenObjects);

        int uppercaseCount = 0;
        int lowercaseCount = 0;
        for (String obj : kitchenObjects) {
            if (Character.isUpperCase(obj.charAt(0))) {
                uppercaseCount++;
            } else {
                lowercaseCount++;
            }
        }
        System.out.println("Elements starts with uppercase = " + uppercaseCount);
        System.out.println("Elements starts with lowercase = " + lowercaseCount);

        int pCount = 0;
        for (String obj : kitchenObjects) {
            if (obj.contains("P") || obj.contains("p")) {
                pCount++;
            }
        }
        System.out.println("Elements having P or p = " + pCount);

        int startEndPCount = 0;
        for (String obj : kitchenObjects) {
            if (obj.startsWith("P") || obj.startsWith("p") || obj.endsWith("P") || obj.endsWith("p")) {
                startEndPCount++;
            }
        }
        System.out.println("Elements starting or ending with P or p = " + startEndPCount);

        System.out.println("\n-----Task-10-----\n");

        ArrayList<Integer> numbersss = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));

        System.out.println(numbersss);

        int divisibleBy10Count = 0;
        for (int num : numbersss) {
            if (num % 10 == 0) {
                divisibleBy10Count++;
            }
        }
        System.out.println("Elements that can be divided by 10 = " + divisibleBy10Count);

        int evenGreaterThan15Count = 0;
        for (int num : numbersss) {
            if (num > 15 && num % 2 == 0) {
                evenGreaterThan15Count++;
            }
        }
        System.out.println("Elements that are even and greater than 15 = " + evenGreaterThan15Count);

        int oddLessThan20Count = 0;
        for (int num : numbersss) {
            if (num < 20 && num % 2 != 0) {
                oddLessThan20Count++;
            }
        }
        System.out.println("Elements that are odd and less than 20 = " + oddLessThan20Count);

        int lessThan15OrGreaterThan50Count = 0;
        for (int num : numbersss) {
            if (num < 15 || num > 50) {
                lessThan15OrGreaterThan50Count++;
            }
        }
        System.out.println("Elements that are less than 15 or greater than 50 = " + lessThan15OrGreaterThan50Count);
    }
}