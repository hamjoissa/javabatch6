package homework;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("\n------Task#1------\n");

        String str = "JAVA";
        System.out.println(countConsonants(str));
        System.out.println("\n------Task#2------\n");

        str = "Java is   fun";

        System.out.println(Arrays.toString(wordArray(str)));

        System.out.println("\n------Task#3------\n");

        System.out.println(removeExtraSpaces(str));

        System.out.println("\n------Task#4------\n");

        System.out.println(count3OrLess());

        System.out.println("\n------Task#5------\n");

        str = "01/21/1999";
        isDateFormatValid(str);

        System.out.println("\n------Task#6------\n");

        str = "hamjoissa@gmail.com";
        System.out.println(isEmailFormatValid(str));

    }

    //Task#1
    public static int countConsonants(String str) {
        String consonants = "[bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ]";
        return str.replaceAll("\\s+", "").replaceAll(consonants, "").length();
    }// Task#1 END





    //Task#2
    public static String[] wordArray(String str) {
        String[] words = str.split("\\s+");

        return words;
    }//Task#2 END




    //Task#3
    public static String removeExtraSpaces(String str) {
        str = str.replaceAll("\\s+", " ");

        str = str.trim();

        return str;
    }//Task#3 END




    //Task#4
    public static int count3OrLess() {

        String sentence = ScannerHelper.getString();

        String[] words = sentence.split("\\s+");

        int count = 0;
        for (String word : words) {
            if (word.matches("\\b\\w{1,3}\\b")) {
                count++;
            }
        }

        return count;
    }//Task#4 END





    //Task#5
    public static boolean isDateFormatValid(String DateOfBirth) {
        // System.out.println("====== task 5 =====");
        if (Pattern.matches("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}", DateOfBirth)) {
            return true;
        }
            else{
            return false;
        }//Task#5 END
    }



    //Task#6
    public static boolean isEmailFormatValid(String email) {
        return email.matches("\\b\\w{2,}@\\w{2,}\\.\\w{2,}\\b");
    }//Task#6 END



}
