package homework;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n---------TASK1---------\n");
/*
J = 01001010  A = 01000001  V = 01010110  A = 01000001


S = 01010011  E = 01000101  L = 01001100  E = 01000101
N = 01001110  I = 01001001  U = 01010101  M = 01001101
 */

        System.out.println("\n---------TASK2---------\n");
/*
01001101    = M
01101000    = h
01111001    = y
01010011    = S
01101100    = l
 */

        System.out.println("\n---------TASK3---------\n");

        System.out.println("I start to practice \"JAVA\" today, and i like it.");

        System.out.println("The secret of getting ahead is getting started.");

        System.out.println("\"Don't limit yourself.\"");

        System.out.println("Invest in your dreams. Grind now. Shine later.");

        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");

        System.out.println("The hard days are what make you stronger");

        System.out.println("You can waste your lives drawing lines. Or you can live your \nlife crossing them.");


        System.out.println("\n---------TASK4---------\n");

        System.out.println("\tJava is easy to write and easy to run—this is the\nfoundational strength of Java and why many developers\nprogram in it. When you write Java once, you can run it\nalmost anywhere at any time.\n\n\tJava can be used to create complete applications\nthat can run on a single computer or be distributed\nacross servers and clients in a network.\n\n\tAs a result, you can use it to easily build mobile\napplications or run-on desktop applications that use\ndifferent operating systems and servers, such as Linux\nor Windows.");


        System.out.println("\n---------TASK5---------\n");

        int myAge = 22;
        int myFavoriteNumber = 2;
        double myHeight = 5.11;
        int myWeight = 320;
        char myFavoriteLetter = 'H';

        System.out.println(myAge);
        System.out.println(myFavoriteNumber);
        System.out.println(myHeight);
        System.out.println(myWeight);
        System.out.println(myFavoriteLetter);


    }
}
