package homework;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n---------------------------TASK#1-------------------------------\n");

        System.out.println("Please enter 2 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));

        System.out.println("\n---------------------------TASK#2-------------------------------\n");

        System.out.println("Please enter 5 numbers");
        int i1 = input.nextInt();
        int i2 = input.nextInt();
        int i3 = input.nextInt();
        int i4 = input.nextInt();
        int i5 = input.nextInt();

        System.out.println("Max value = " + Math.max(Math.max(Math.max(i1, i2), Math.max(i3, i4)), i5));
        System.out.println("Min value = " + Math.min(Math.min(Math.min(i1, i2), Math.min(i3, i4)), i5));

        System.out.println("\n---------------------------TASK#3-------------------------------\n");
        int number1 = ((int) (Math.random() * 50) + 50);
        int number2 = ((int) (Math.random() * 50) + 50);
        int number3 = ((int) (Math.random() * 50) + 50);


        System.out.println("Number 1 = " + number1);

        System.out.println("Number 2 = " + number2);

        System.out.println("Number 3 = " + number3);

        System.out.println("The sum of numbers is = " + (number1 + number2 + number3));

        System.out.println("\n---------------------------TASK#4-------------------------------\n");

        double alex = 125, mike = 220;

        System.out.println("Alex's money: $" + (alex - 25.5));
        System.out.println("Mike's money: $" + (mike + 25.5));

        System.out.println("\n---------------------------TASK#5-------------------------------\n");

        double price = 390;
        double save = 15.60;
        int days = (int) (price / save);
        System.out.println(days);

        System.out.println("\n---------------------------TASK#6-------------------------------\n");

        String s1 = "5", s2 = "10";

        int sum = (Integer.parseInt(s1)) + (Integer.parseInt(s2));
        int product = (Integer.parseInt(s1)) * (Integer.parseInt(s2));
        int division = (Integer.parseInt(s1)) / (Integer.parseInt(s2));
        int subtraction = (Integer.parseInt(s1)) - (Integer.parseInt(s2));
        int remainder = (Integer.parseInt(s1)) % (Integer.parseInt(s2));

        System.out.println("-Sum of 5 and 10 is = " + sum);
        System.out.println("-Product of 5 and 10 is = " + product);
        System.out.println("-Division of 5 and 10 is = " + division);
        System.out.println("-Subtraction of 5 and 10 is = " + subtraction);
        System.out.println("-Remainder of 5 and 10 is = " + remainder);

        System.out.println("\n---------------------------TASK#7-------------------------------\n");

        String ss1 = "200", ss2 = "-50";

        int max = Math.max(Integer.parseInt(ss1), Integer.parseInt(ss2));

        int min = Math.min(Integer.parseInt(ss1), Integer.parseInt(ss2));

        int average = (Integer.parseInt(ss1) + Integer.parseInt(ss2)) / 2;

        int absolute = Math.abs(Integer.parseInt(ss1) + Math.abs(Integer.parseInt(ss2)));

        System.out.println("The greatest value is = " + max);
        System.out.println("The smallest value is = " + min);
        System.out.println("The average is = " + average);
        System.out.println("The absolute difference is = " + absolute);

        System.out.println("\n---------------------------TASK#8-------------------------------\n");


        int day1 = 24, day2 = 168, day3 = 150;
        double coins = .96;

        int save1 = (int) (day1 / coins);
        int save2 = (int) (day2 / coins);


        System.out.println(save1 + " days ");
        System.out.println(save2 + " days ");
        System.out.println("$" + (coins) * (day3));

        System.out.println("\n---------------------------TASK#9-------------------------------\n");

        double saves = 62.5;

        int computer = 1250;

        int days1 = (int) (computer / saves);

        System.out.println(days1);

        System.out.println("\n---------------------------TASK#10-------------------------------\n");

        double option1 = 475.50;

        int option2 = 951;

        int result1 = (int) (14265 / option1);
        int result2 = 14265 / option2;

        System.out.println("Option 1 will take " + result1 + " months");

        System.out.println("Option 2 will take " + result2 + " months");

        System.out.println("\n---------------------------TASK#11-------------------------------\n");

        System.out.println("Please enter 2 number");
        int a1 = input.nextInt();
        int a2 = input.nextInt();

        double a3 = Double.parseDouble(String.valueOf(a1)) / Double.parseDouble(String.valueOf(a2));

        System.out.println(a3);

        System.out.println("\n---------------------------TASK#12-------------------------------\n");


        int random1 = ((int) (Math.random() * 100));
        int random2 = ((int) (Math.random() * 100));
        int random3 = ((int) (Math.random() * 100));


        if (random1 >= 25 && random2 >= 25 && random3 >= 25)

            System.out.println("True if all numbers are greater than 25");

        else {

            System.out.println("False if any of the numbers is less than or equals 25");

        }

        System.out.println("\n---------------------------TASK#13-------------------------------\n");

        System.out.println("Please enter a number between 1 to 7");
        int n1 = input.nextInt();

        if (n1 == 1) {
            System.out.println("Sunday");
        } else if (n1 == 2) {
            System.out.println("Monday");
        } else if (n1 == 3) {
            System.out.println("Tuesday");
        } else if (n1 == 4) {
            System.out.println("Wednesday");
        } else if (n1 == 5) {
            System.out.println("Thursday");
        } else if (n1 == 6) {
            System.out.println("Friday");
        } else if (n1 == 7) {
            System.out.println("Saturday");

        }

        System.out.println("\n---------------------------TASK#14-------------------------------\n");

        System.out.println("Tell me your exam results?");
        int score1 = input.nextInt();
        int score2 = input.nextInt();
        int score3 = input.nextInt();

        int finalscore = (score1 + score2 + score3) / 3;

        if (finalscore > 70)
            System.out.println("YOU PASSED!");

        else
            System.out.println("YOU FAILED!");


        System.out.println("\n---------------------------TASK#15-------------------------------\n");

        System.out.println("Please enter 3 numbers");
        int u1 = input.nextInt();
        int u2 = input.nextInt();
        int u3 = input.nextInt();


        if (u1 == u2 && u2 == u3) {

            System.out.println("TRIPLE MATCH");
        } else if (u1 == u2 || u2 == u3 || u1 == u3) {

            System.out.println("DOUBLE MATCH");
        } else {

            System.out.println("NO MATCH");
        }


    }
}