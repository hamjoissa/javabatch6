package homework;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n-------------Task#1---------------\n");

        int[] numbers = new int[10];

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n-------------Task#2---------------\n");

        String[] letters = new String[5];

        letters[1] = "abc";
        letters[4] = "xyz";

        System.out.println(letters[3]);
        System.out.println(letters[0]);
        System.out.println(letters[4]);
        System.out.println(Arrays.toString(letters));

        System.out.println("\n-------------Task#3---------------\n");

        int[] numberss = {23, -35, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numberss));

        Arrays.sort(numberss);

        System.out.println(Arrays.toString(numberss));

        System.out.println("\n-------------Task#4---------------\n");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));

        System.out.println("\n-------------Task#5---------------\n");

        String[] cartoondogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoondogs));

        for (String cartoondog : cartoondogs) {
            if (cartoondog.contains("Pluto")){
                System.out.println("True");
            break;}
        }

        System.out.println("\n-------------Task#6---------------\n");

        String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        System.out.println(Arrays.toString(cats));

        for (String cat : cats) {
            if (cat.contains("Garfield") && cat.contains("Felix"))
                System.out.println("True");
            else System.out.println("false");break;
        }

        System.out.println("\n-------------Task#7---------------\n");

        double[] numbersss ={10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(numbersss));

        for (double v : numbersss) {
            System.out.println(v);
        }

        System.out.println("\n-------------Task#8---------------\n");

        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(characters));

        int letterCount = 0;
        int uppercaseCount = 0;
        int lowercaseCount = 0;
        int digitCount = 0;
        int specialCount = 0;

        for (char c : characters) {
            if (Character.isLetter(c)) {
                letterCount++;
                if (Character.isUpperCase(c)) {
                    uppercaseCount++;
                } else if (Character.isLowerCase(c)) {
                    lowercaseCount++;
                }
            } else if (Character.isDigit(c)) {
                digitCount++;
            } else {
                specialCount++;
            }
        }

        System.out.println("Letters = " + letterCount);
        System.out.println("Uppercase letters = " + uppercaseCount);
        System.out.println("Lowercase letters = " + lowercaseCount);
        System.out.println("Digits = " + digitCount);
        System.out.println("Special characters = " + specialCount);

        System.out.println("\n-------------Task#9---------------\n");

        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(objects));

        int uppercaseCount2 = 0;
        int lowercaseCount2 = 0;
        int bpCount = 0;
        int bookPenCount = 0;

        for (String s : objects) {
            if (Character.isUpperCase(s.charAt(0))) {
                uppercaseCount2++;
            } else if (Character.isLowerCase(s.charAt(0))) {
                lowercaseCount2++;
            }
            if (s.toLowerCase().startsWith("b") || s.toLowerCase().startsWith("p")) {
                bpCount++;
            }
            if (s.toLowerCase().contains("book") || s.toLowerCase().contains("pen")) {
                bookPenCount++;
            }
        }

        System.out.println("Elements starts with uppercase = " + uppercaseCount2);
        System.out.println("Elements starts with lowercase = " + lowercaseCount2);
        System.out.println("Elements starting with B or P = " + bpCount);
        System.out.println("Elements having \"book\" or \"pen\"= " + bookPenCount);

        System.out.println("\n-------------Task#10---------------\n");

        int[] numberssss = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(numberssss));

        int moreThanTenCount = 0;
        int lessThanTenCount = 0;
        int equalToTenCount = 0;

        for (int num : numberssss) {
            if (num > 10) {
                moreThanTenCount++;
            } else if (num < 10) {
                lessThanTenCount++;
            } else {
                equalToTenCount++;
            }
        }

        System.out.println("Elements that are more than 10 = " + moreThanTenCount);
        System.out.println("Elements that are less than 10 = " + lessThanTenCount);
        System.out.println("Elements that are 10 = " + equalToTenCount);

        System.out.println("\n-------------Task#11---------------\n");

        int[] first = {5, 8, 13, 1, 2};
        int[] second = {9, 3, 67, 1, 0};

        System.out.println("1st array is =  " + Arrays.toString(first));
        System.out.println("2nd array is = " + Arrays.toString(second));

        int[] greatest = new int[first.length];

        for (int i = 0; i < first.length; i++) {
            greatest[i] = Math.max(first[i], second[i]);
        }

        System.out.println("3rd array is =  " + Arrays.toString(greatest));





    }
}
