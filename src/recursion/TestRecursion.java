package recursion;

public class TestRecursion {

    public static void main(String[] args) {
        System.out.println(sum1ToNIterative(5)); // 15
        System.out.println(sum1ToNIterative(4)); // 10
        System.out.println(sum1ToNIterative(10)); // 55

        System.out.println(sum1ToNRecursive(4)); // 10
        System.out.println(sum1ToNRecursive(5)); // 15
        System.out.println(sum1ToNRecursive(10)); // 55
    }


    /*
     Create iterative and recursive methods that are used to find the sum of
     the numbers from 1 to n

     n = 5   -> 15
     n = 4   -> 10
     */

    public static int sum1ToNIterative(int n){
        int sum = 0;

        for (int i = 1; i <= n; i++) {
            sum += i;
        }

        return sum;
    }

    public static int sum1ToNRecursive(int n){
        if(n != 1) return n + sum1ToNRecursive(n-1);
        return 1;
    }

    public static int factorialRecursion(int n) {
        if (n == 1) {
            return 1;
        }
        else {
            return n * factorialRecursion(n-1);
        }
    }

}