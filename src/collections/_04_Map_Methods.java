package collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class _04_Map_Methods {
    public static void main(String[] args) {
        HashMap<String, String> capitals = new HashMap<>();

        System.out.println("\n---------How to add entries to a map---------\n");
        capitals.put("France", "Paris");
        capitals.put("Italy", "Rome");
        capitals.put("Spain", "Madrid");


        System.out.println("\n-----------How to print a map------------\n");
        System.out.println(capitals);


        System.out.println("\n-------------------How to retrieve a value--------------\n");
        System.out.println(capitals.get("France"));
        System.out.println(capitals.get("Italy"));
        System.out.println(capitals.get("Spain"));

        System.out.println(capitals.get("USA"));
        System.out.println(capitals.get("france"));


        System.out.println("\n-----------How to check if map contains given key or value------------\n");
        System.out.println(capitals.containsKey("Italy"));
        System.out.println(capitals.containsKey("Belgium"));

        System.out.println(capitals.containsValue("Rome"));
        System.out.println(capitals.containsValue("madrid"));


        System.out.println("\n-----------How to remove some entries------------\n");
        System.out.println(capitals.remove("USA"));
        System.out.println(capitals.remove("Italy", "rome"));

        System.out.println(capitals.remove("Italy"));
        System.out.println(capitals.remove("Spain", "Madrid"));
        System.out.println(capitals);

        System.out.println("\n-----------How to get the size of the map------------\n");
        System.out.println(capitals.size());

        capitals.put("Ukraine", "Kyiy");
        capitals.put("Turkiye", "Ankara");

        System.out.println("\n-----------How to get all keys------------\n");
        System.out.println(capitals.keySet());

        for (String key : capitals.keySet()) {
            System.out.println(key);
        }

        System.out.println("\n-----------How to get all values------------\n");
        System.out.println(capitals.values());

        Collection<String> values = capitals.values();

        for (String value : values) {
            System.out.println(value);
        }

        System.out.println("\n-----------How to get all entries------------\n");
        System.out.println(capitals.entrySet());

        Set<Map.Entry<String, String>> entries = capitals.entrySet();

        for (Map.Entry<String, String> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }



    }
}
