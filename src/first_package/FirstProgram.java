package first_package;

import singleton.Phone;

public class FirstProgram {
    public static void main(String[] args) {
        System.out.println("This Is TechGlobal");
        System.out.println("I am learning Java");
        System.out.println("My name is Hamza");

        //More practices

        Phone phone = Phone.getPhone();

        System.out.println(phone);

    }


}
