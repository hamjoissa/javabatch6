package Mock_Practices;

import utilities.ScannerHelper;

public class Practice03_Task2 {
    public static void main(String[] args) {
        Middle(ScannerHelper.getString());

    }
    public static void Middle(String str) {
        if (str.length() < 3) {
            System.out.println("Length is less than 3");
        } else {
            if (str.length() % 2 == 1) {
                System.out.println("Middle character: " + str.charAt(str.length() / 2));
            } else {
                System.out.println("Middle characters: " + str.charAt(str.length() / 2 - 1) + str.charAt(str.length() / 2));
            }
        }
    }
}
