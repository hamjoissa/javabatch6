package Mock_Practices;

import utilities.RandomGenerator;
import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.Random;

public class SandBox {
    public static void main(String[] args) {
        int num = ScannerHelper.getNumber();

        String[] list = {"Tech", "Global", "Global", "Tech"};
        System.out.println(Arrays.toString(fib(num)));

        //System.out.println(Arrays.toString(removeDuplicates(list)));

        Random r = new Random();

        int num1 = r.nextInt(10) + 1;
        int num2 = r.nextInt(10) + 1;
        int num3 = r.nextInt(10) + 1;
        int num4 = r.nextInt(10) + 1;
        int num5 = r.nextInt(10) + 1;

        for (String s : list) {

        }
    }

    public static int[] fib(int n) {
        int[] fibonacci = new int[n];
        int a = 0, b = 1;
        fibonacci[0] = a;
        fibonacci[1] = b;
        for (int i = 2; i < n; i++) {
            fibonacci[i] = a + b;
            a = b;
            b = fibonacci[i];
        }
        return fibonacci;
    }

    public static void Findvowels(String str) {

        if (str.length() == 0) {
            System.out.println("Length is zero");
        } else {

            System.out.println("Length is = " + str.length());
            System.out.println("First char is = " + str.charAt(0));
            System.out.println("Last char is = " + str.charAt(str.length() - 1));

            if (str.toLowerCase().contains("a") || str.contains("e") || str.contains("i") || str.contains("o") ||
                    str.contains("u")) {
                System.out.println("This String has vowel");
            } else System.out.println("This String has no vowel");
        }
    }

    public static String[] swapEnds(String[] str) {

        str[0] = str[str.length - 1];

        str[str.length - 1] = str[0];

        return str;
    }

    public static int[] removeDuplicates(int[] arr) {
        if (arr == null || arr.length == 0) {
            return arr;
        }

        int[] result = new int[arr.length];
        int index = 0;

        for (int i = 0; i < arr.length; i++) {
            boolean isDuplicate = false;

            // check if the current element has already appeared before
            for (int j = 0; j < index; j++) {
                if (arr[i] == result[j]) {
                    isDuplicate = true;
                    break;
                }
            }

            // add the current element to the result array if it's not a duplicate
            if (!isDuplicate) {
                result[index++] = arr[i];
            }
        }

        // copy the non-duplicate elements to a new array with the correct size
        int[] trimmedResult = new int[index];
        System.arraycopy(result, 0, trimmedResult, 0, index);

        return trimmedResult;
    }

    public static int countMultiWords(String[] arr) {
        int count = 0;
        for (String str : arr) {
            if (str.split(" ").length > 1) {
                count++;
            }
        }
        return count;
    }
}
