package Mock_Practices;

import java.util.Random;

public class Practices02_task2 {
    public static void main(String[] args) {
        Random r = new Random();

        int num = r.nextInt(100) + 1;

        if (num < 25)
            System.out.println("1st quarter");
        else if (num >= 26 && num <= 50) {
            System.out.println("2nd quarter");
        } else if (num >= 51 && num <= 75) {
            System.out.println("3rd quarter");
        } else System.out.println("4th quarter ");

        if (num < 50)
            System.out.println("1st half");else System.out.println("2nd half");
    }
}
