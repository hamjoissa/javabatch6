package Mock_Practices;

import java.util.Scanner;

public class Practice04_Task1 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            if (i % 2 == 0 && i % 5 == 0)
                System.out.println("FooBar");
            else if (i % 5 == 0) {
                System.out.println("Bar");
            } else if (i % 2 == 0) {
                System.out.println("Foo");
            }else System.out.println(i);
        }

        System.out.println("\n------------palindrome------------\n");
        String original, reverse = ""; // Objects of String class
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a string to check if it is a palindrome");
        original = in.nextLine();
        for ( int i = original.length() - 1; i >= 0; i-- )
            reverse = reverse + original.charAt(i);
        if (original.equals(reverse))
            System.out.println("Entered string is a palindrome.");
        else
            System.out.println("Entered string isn't a palindrome.");
    }
    public static int[] fib(int n) {
        int[] fibonacci = new int[n];
        int a = 0, b = 1;
        fibonacci[0] = a;
        fibonacci[1] = b;
        for (int i = 2; i < n; i++) {
            fibonacci[i] = a + b;
            a = b;
            b = fibonacci[i];
        }
        return fibonacci;
    }
}
