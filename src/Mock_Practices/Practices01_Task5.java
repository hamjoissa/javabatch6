package Mock_Practices;

import java.util.Random;

public class Practices01_Task5 {
    public static void main(String[] args) {
        Random r = new Random();

        int num1 = r.nextInt(25) + 1;
        int num2 = r.nextInt(27) + 23;
        int num3 = r.nextInt(52) + 23;
        int num4 = r.nextInt(77) + 23;

        int max = Math.max(num1, Math.max(num2, Math.max(num3, num4)));
        int min = Math.min(num1, Math.min(num2, Math.min(num3, num4)));
        int average = (num1 + num2 + num3 + num4) / 4;

        System.out.println("Difference of max and min = " + Math.abs(max - min));

        System.out.println("Difference of second and third = " + Math.abs(num2 - num3));

        System.out.println("Average of all = " + average);

    }
}
