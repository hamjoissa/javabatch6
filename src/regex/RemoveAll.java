package regex;

public class RemoveAll {
    public static void main(String[] args) {
        String str = "Apple";

        int vowelCounter = 0;
        for (char c : str.toCharArray()) {
            if (Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' || Character.toLowerCase(c) == 'i' || Character.toLowerCase(c) == 'o' || Character.toLowerCase(c) == 'u') {
                vowelCounter++;
            }
        }
        System.out.println("The word has " + vowelCounter + "vowels");

        str = str.replaceAll("[^aeiouAEIOU]", "");
        System.out.println("The word has " + str.length() + "vowels");
    }
}
