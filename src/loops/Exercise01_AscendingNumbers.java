package loops;

import utilities.ScannerHelper;

public class Exercise01_AscendingNumbers {
    public static void main(String[] args) {
        for (int i = ScannerHelper.getNumber(); i < 100; i++){
            System.out.println(i);
        }
    }
}
