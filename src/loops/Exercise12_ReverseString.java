package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {

        String name = ScannerHelper.getFirstName();

        String reversedName = "";

        for (int i = name.length()-1; i >= 0 ; i--) { // nhoj
            reversedName += name.charAt(i);
        }
        System.out.println(reversedName);
    }
}
