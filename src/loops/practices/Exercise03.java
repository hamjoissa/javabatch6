package loops.practices;

import utilities.ScannerHelper;

public class Exercise03 {
    public static void main(String[] args) {
        String fullName = ScannerHelper.getFullName().toLowerCase();//John Doe
        int countOfVowels = 0;

        for(int i = 0; i < fullName.length(); i++){
            if(fullName.charAt(i) == 'a' ||fullName.charAt(i) == 'e' ||
                    fullName.charAt(i) == 'i'||fullName.charAt(i) == 'o' || fullName.charAt(i) == 'u') countOfVowels++;
        }
        System.out.println("There are " + countOfVowels + " vowel letters in this full name");
    }
}
