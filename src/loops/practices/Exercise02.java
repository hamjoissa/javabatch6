package loops.practices;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        int n = ScannerHelper.getNumber();

        while (n <= 10) {
            ScannerHelper.getNumber();
        }
        System.out.println("End of Program");
    }
}

