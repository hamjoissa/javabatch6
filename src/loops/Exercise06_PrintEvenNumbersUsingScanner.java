package loops;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {

        int i1 = ScannerHelper.getNumber();
        int i2 = ScannerHelper.getNumber();

        int iMax = Math.max(i1, i2);
        int iMin = Math.min(i1, i2);

        for (int i = iMin; i < iMax + 1; i++) {
            if (i % 2 == 0) System.out.println(i);

        }

    }
}
