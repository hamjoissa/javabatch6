package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {

        /*
        for loop syntax

        for(initialization; termination; update){
        //blockof code to be executed
        }

         */

        for(int num = 0; num < 5; num++){
            System.out.println("Hello World");
        }
    }
}
