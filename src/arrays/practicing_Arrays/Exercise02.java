package arrays.practicing_Arrays;

public class Exercise02 {
    public static void main(String[] args) {
        String[] arr = {"red", "blue", "yellow", "white"};
        getShortestLongest(arr);


    }

    public static void getShortestLongest(String[] words){

        String shortest = words[0];
        String longest = words[0];

        for (String word : words) {
            if (word.length() < shortest.length()) {
                shortest = word;
            } else if (word.length() > longest.length()) {
                longest = word;

            }
        }

        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);
    }
}
