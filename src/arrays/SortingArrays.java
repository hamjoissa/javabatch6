package arrays;

import java.util.Arrays;

public class SortingArrays {
    public static void main(String[] args) {
        String[] words = {"Alex", "ali", "John", "James"};
        int[] numbers = {5,3,10};

        Arrays.sort(numbers);
        Arrays.sort(words);

        System.out.println(Arrays.toString(numbers));
        System.out.println(Arrays.toString(words));
    }
}
