package arrays;

public class NullKeyword {
    public static void main(String[] args) {

        String s1 = null;
        String s2 = "Hamza";

        System.out.println(s1);// null
        System.out.println(s2);// Hamza

        /*
        System.out.println(s1.length());// NullPointerException
        System.out.println(s1.charAt(1));// null
        System.out.println(s2.length());// 5
         */
        Double[] d = new Double[2];
        System.out.println(d);
    }
}
