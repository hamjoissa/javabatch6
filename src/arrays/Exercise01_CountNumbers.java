package arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {

        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};
        int count = 0;

        for (int number : numbers) {
            if (number < 0)
                count++;
        }System.out.println(count);

        int counting = 0;


        for (int number : numbers) {
            if (number % 2 == 0)
                counting++;
        }System.out.println(counting);


        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);
    }
}
