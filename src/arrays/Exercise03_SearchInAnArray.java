package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

        /*
        Check the collection you have above and print true if it contains Mouse
        Print false otherwise

        RESULT:
        true
        */

        boolean hasMouse = false;
        for (String object : objects) {
            if (object.contains("Mouse")) {
                hasMouse = true;
                break;
            }
        }
        System.out.println(hasMouse);

        System.out.println("\n------------binary way------------\n");

        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0); // true
        System.out.println(Arrays.binarySearch(objects, "mouse") >= 0); // false
        System.out.println(Arrays.binarySearch(objects, "Keyword") >= 0); // true
        System.out.println(Arrays.binarySearch(objects, "board") >= 0); // false


    }
}
