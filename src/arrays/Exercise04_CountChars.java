package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {

            String str = ScannerHelper.getString();

            char[] charsOfStr = str.toCharArray();
            int letters = 0;
            for (int i = 0; i < str.length(); i++) {
                if (Character.isLetter(str.charAt(i)))  letters++;
            }

        System.out.println(letters);

    }
}

