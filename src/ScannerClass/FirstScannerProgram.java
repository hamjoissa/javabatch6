package ScannerClass;

import java.util.Scanner;

public class FirstScannerProgram {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        //.next method
        System.out.println("Please enter your name");
        String name = input.next(); // returns us with a String of the user choice and assigns it to the String name
        input.next();
        System.out.println("the user's name is: " + name); // prints out the user's name is: {name}

        System.out.println("\n--------------------------------------\n");

        //.nextline method

        System.out.println("Please enter your first and last name");
        String Fullname = input.nextLine();
        System.out.println("The users full name is: " + Fullname);
    }
}
