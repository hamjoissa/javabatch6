package ScannerClass;

import java.util.Scanner;

public class ScannerAddingNumbers {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int num1, num2, num3;

        System.out.println("Please enter your first number");
        num1 = input.nextInt();

        System.out.println("Please enter your second number");
        num2 = input.nextInt();

        System.out.println("Please enter your third number");
        num3 = input.nextInt();

        System.out.println("The sum of the numbers you entered is " + (num1 + num2 + num3));


    }
}
