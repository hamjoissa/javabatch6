package ScannerClass;

import java.util.Scanner;

public class ScannerFirstandLastname {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your first name?");

        String Firstname = input.next();

        System.out.println("What is your last name?");

        String Lastname = input.next();

        System.out.println("Your full name is " + Firstname + " " + Lastname);

        //nextInt method

        System.out.println("Please enter a number");
        int number = input.nextInt();
        System.out.println("The number you chose is: " + number);


    }
}
