package ScannerClass;

import java.util.Scanner;

public class Recap01 {
    public static void main(String[] args) {
        /*
        write a program that asks user to enter their firstname, address, favNumber

        Then, print them all in a way as below
        {firstname}'s address is {address} and their fav number is {favNumber}.

        Firstname = john
        Address = Chicago IL
        FavNumber = 7;

        John's address is chicago il and their fav number is 7.
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your name?");

        String name = input.next();

        System.out.println("Please enter your Address?");

        String address = input.next();
        input.nextLine();

        System.out.println("Please enter your Favorite number?");

        int favNumber = input.nextInt();

        System.out.println(name +"'s address is " + address + " and their fav number is " + favNumber);
    }
}
