package string_Methods;

import java.util.Scanner;

public class _12_StartsAndEndsWith_Method {
    public static void main(String[] args) {

        String str = "TechGlobal";

        boolean startswith = str.startsWith("T");
        boolean endswith = str.endsWith("T");

        System.out.println(startswith);
        System.out.println(endswith);

        System.out.println(str.startsWith("Techgl")); // false
        System.out.println(str.endsWith("TechGlobal")); // true


    }
}
