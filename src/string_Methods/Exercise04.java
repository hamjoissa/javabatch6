package string_Methods;

import java.util.Scanner;

public class Exercise04 {
    public static void main(String[] args) {
        String i1 = "I go to TechGlobal";
        String i2 = i1.substring(0,1);
        String i3 = i1.substring(2,4);
        String i4 = i1.substring(5,7);
        String i5 = i1.substring(8);

        System.out.println(i2);
        System.out.println(i3);
        System.out.println(i4);
        System.out.println(i5);
    }
}
