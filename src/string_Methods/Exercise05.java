package string_Methods;

import java.util.Scanner;

public class Exercise05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a word");
        String text = input.nextLine();


        if (text.startsWith("a") || text.startsWith("A") && text.endsWith("e") || text.endsWith("E"))
            System.out.println("True");
        else System.out.println("False");

    }
}

