package string_Methods;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter the name of your favorite book");
        String favBook = input.nextLine();

        System.out.println("Please enter your favorite quote");
        String quote = input.nextLine();

        System.out.println("Your favorite book length is " + favBook.length());
        System.out.println("Your favorite quote length is " + quote.length());
    }
}
