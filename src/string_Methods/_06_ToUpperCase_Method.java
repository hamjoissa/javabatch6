package string_Methods;

public class _06_ToUpperCase_Method {
    public static void main(String[] args) {

        System.out.println("Hello World".toUpperCase());

        System.out.println(" ".toUpperCase());

        String s1 = "HELLO";
        String s2 = "hello";

        if (s1.toUpperCase().equals(s2.toUpperCase())) {
            System.out.println("EQUAL");
        } else System.out.println("NOT EQUAL");
    }
}
