package string_Methods;

import utilities.ScannerHelper;

public class _13_Contain_Method {
    public static void main(String[] args) {

        String name = "John Doe";

        boolean containsJohn = name.contains("John");
        System.out.println(containsJohn);


        String  text = ScannerHelper.getString();

        if (text.contains(" ") && text.contains(".")) System.out.println("True");
        else System.out.println("False");


    }
}
