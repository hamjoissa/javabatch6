package string_Methods;

import utilities.ScannerHelper;


public class Exercise01 {
    public static void main(String[] args) {

        String i1 = ScannerHelper.getString();
        String i2 = ScannerHelper.getString();

        if (i1.equals(i2))
            System.out.println("These strings are equal");
        else
            System.out.println("These strings are not equal");
    }
}
