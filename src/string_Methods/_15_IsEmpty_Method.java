package string_Methods;

public class _15_IsEmpty_Method {
    public static void main(String[] args) {
        /*
        1. return type
        2. return boolean
        3. Non-Static
        4. no arguments
         */
        String emptyStr = "";
        String word = "Hello";

        System.out.println("First String is empty = " + emptyStr.isEmpty()); //true
        System.out.println("Second String is empty = " + word.isEmpty()); //false
    }
}
