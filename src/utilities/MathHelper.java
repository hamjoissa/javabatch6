package utilities;

import java.util.Random;

public class MathHelper {

    //Write a public static method named max() and returns the greatest number of 3 numbers
    /*
        return or void?                     ->   return
        static or non-static                -> static
        what is the name of the method      -> max()
        what it returns?                    ->  int
        Does it take arguments?             -> 3 int arguments
     */

    public static int max(int a, int b, int c) {
        return Math.max(Math.max(a, b), c);
    }

    public static int sum(int a, int b) {

        return (a + b);
    }
    public static double sum(double a, double b) {

        return (a + b);
    }
    public static int sum(int a, int b, int c) {

        return (a + b + c);
    }
    public static long sum(long a, long b) {

        return (a + b);
    }

    public static int product(int a, int b) {

        return (a * b);
    }

    public static double square(double a) {

        return Math.sqrt(a);
    }

    public static int min(int a, int b, int c) {
        return Math.min(Math.max(a, b), c);
    }

}