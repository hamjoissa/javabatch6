package utilities;

import java.util.Scanner;

public class ScannerHelper {
    // write a method that ask and return a name from user

    static Scanner input = new Scanner(System.in);


    public static String getFirstName(){
        System.out.println("Please enter you first name:");
        return input.nextLine();
    }

    public static String getLastName(){
        System.out.println("Please enter you last name:");
        return input.nextLine();
    }

    public static int getAge(){
        System.out.println("Please enter your age:");
        int age = input.nextInt();
        input.nextLine();

        return age;
    }
    public static int getNumber() {
        System.out.println("Please enter a number:");
        int number = input.nextInt();
        input.nextLine();

        return number;
    }


    public static String getString(){
        System.out.println("Please enter a String");
        String str = input.nextLine();

        return str;
    }
    public static String getFavCountry(){
        System.out.println("Please enter your favorite country");
        String str = input.nextLine();

        return str;
    }
    public static String getAddress(){
        System.out.println("Please enter your address");
        String str = input.nextLine();

        return str;
    }
    public static String getFullName(){
        System.out.println("Please enter you full name:");
        return input.nextLine();
    }

}
