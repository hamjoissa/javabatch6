package gitDemo;

public class Demo {
    public static void main(String[] args) {
        String firstname = "Hamza ";
        String lastname = "Issa";

        System.out.println(firstname + lastname);

        String favColor = "Green";
        System.out.println("My favorite color is " + favColor);

        int year = 2023;
        String favCar = "Go Cart";
        System.out.println("My favorite car is " + favCar + " and the model year is " + year);
    }
}
