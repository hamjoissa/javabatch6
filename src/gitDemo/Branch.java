package gitDemo;

public class Branch {
    public static void main(String[] args) {
        /*
        Branching: we create branches for individual members, so they can get started with coding
                    Ex) feature/teamname/userstory#-context
                        featureAutomationTester/US1307-Retry-Functionality

         1) git branch nameofthebranch
         2) git checkout -b nameofthebranch

            /*
                I Love Weed
         */

    }
}
