package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class _11_Remove_Elements_Iterator {
    public static void main(String[] args) {
        //Remove all the elements that are more than 10 -> [10, 5, 3, 0]

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 15, 12, 5, 3, 0, 30));


    }
    public static ArrayList<String> removeVowels3(ArrayList<String> list){
        Iterator<String> iterator = list.iterator();

        while(iterator.hasNext()){
            String str = iterator.next();
            if(str.toLowerCase().contains("a") || str.toLowerCase().contains("e")|| str.toLowerCase().contains("i") ||
                    str.toLowerCase().contains("o")|| str.toLowerCase().contains("u")) iterator.remove();
        }
        return list;
    }
}