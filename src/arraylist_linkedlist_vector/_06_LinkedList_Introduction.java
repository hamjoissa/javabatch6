package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class _06_LinkedList_Introduction {
    public static void main(String[] args) {
        LinkedList<String> cities = new LinkedList<>(Arrays.asList("Berlin", "Rome", "Kyiy", "Ankara", "Madrid", "Chicago"));

        System.out.println(cities.size()); // 6
        System.out.println(cities.contains("Miami")); // false
        System.out.println(cities.indexOf("Evanston")); // -1

        System.out.println(cities.getFirst()); // berlin
        System.out.println(cities.getLast()); // Chicago

        System.out.println(cities.removeFirst()); // Berlin
        System.out.println(cities.removeLast()); // Chicago

        System.out.println(cities);

        System.out.println(cities.pop());

        System.out.println(cities);

        cities.push("Barcelona");

        System.out.println(cities);

        System.out.println(cities.offer("Gent")); // true
    }

    public static int counteven(ArrayList<Integer> list) {
        int count = 0;
        for (Integer num : list) {
            if (num % 2 == 0) {
                count++;
            }
        }
        return count;
    }
}
