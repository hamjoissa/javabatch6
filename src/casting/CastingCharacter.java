package casting;

public class CastingCharacter {
    public static void main(String[] args) {

        int i1 = 65;

        char c1 = (char) i1;

        System.out.println(c1); // A

        char c2 = 97; // a

        System.out.println(c2);
    }
}
