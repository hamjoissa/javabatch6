package conditional_statements;

import java.util.Random;

public class Exercise07_DaysOfTheWeek {
    public static void main(String[] args) {
        Random r = new Random();
        int n1 = r.nextInt(7);

        if (n1 == 0) {
            System.out.println("Sunday");
        } else if (n1 == 1) {
            System.out.println("Monday");
        } else if (n1 == 2) {
            System.out.println("Tuesday");
        } else if (n1 == 3) {
            System.out.println("Wednesday");
        } else if (n1 == 4) {
            System.out.println("Thursday");
        } else if (n1 == 5) {
            System.out.println("Friday");
        } else if (n1 == 6) {
            System.out.println("Saturday");

        }
    }}