package conditional_statements;

import com.sun.org.apache.bcel.internal.generic.SWITCH;

import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number from 1 to 6");
        int num = input.nextInt();

        if (num == 1) {
            System.out.println("Jan");
        } else if (num == 2) {
            System.out.println("Feb");
        } else if (num == 3) {
            System.out.println("Mar");
        } else if (num == 4) {
            System.out.println("Apr");
        } else if (num == 5) {
            System.out.println("May");
        } else if (num == 6) {
            System.out.println("Jun");
        } else {
            System.out.println("This is not a number from 1 to 6");
        }


        switch (num) {
            case 1: {
                System.out.println("Jan");
                break;
            }
            case 2: {
                System.out.println("Feb");
                break;
            }
            case 3: {
                System.out.println("Mar");
                break;
            }
            case 4: {
                System.out.println("Apr");
                break;
            }
            case 5: {
                System.out.println("May");
                break;
            }
            case 6: {
                System.out.println("Jun");
                break;
            }
            default: {
                System.out.println("This is not a number from 1 to 6");
            }

        }


        System.out.println("please enter either a, b, or c");
        input.nextLine();
        String letter = input.nextLine();

        switch (letter) {
            case "a": {
                System.out.println("The letter you entered is a");
                break;
            }
            case "b": {
                System.out.println("The letter you entered is b");
                break;
            }
            case "c": {
                System.out.println("The letter you entered is c");
                break;
            }
            default: {
                System.out.println("ERROR! This is not a letter from a to c");
            }
             /*
Requirement:
    -Assume you are given a single character. (It will be hard-coded)
    -If given char is a letter, then print "Character is a letter"
    -If given char is a digit, then print "Character is a digit"
    USE ASCII TABLE for this task
    Test data:
    'v'
    Expected result:
    Character is a letter
    Test data:
    '5'
    Expected result:
    Character is a digit
 */

            char c = '7';

            if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) {
                System.out.println("Character is a letter");
            } else if (c >= 48 && c <= 57) System.out.println("Character is a digit");
        }


    }
}