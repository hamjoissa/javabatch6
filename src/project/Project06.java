package project;


import java.util.Arrays;

public class Project06 {


    public static void main(String[] args) {

        System.out.println("\n------------TASK-1------\n");
        int[] numbers = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(numbers);

        System.out.println("\n------------TASK-2------\n");
        int[] numberss = {10, 7, 7, 10 - 3, 10, -3};


        int min = 0;
        int max = 0;
        for (int i = 1; i < numberss.length; i++) {

            if (numberss[i] < min) {
                min = numberss[i];
            }
            if (numberss[i] > max) {
                max = numberss[i];
            }
        }
        System.out.println("Smallest = " + min);
        System.out.println("Greatest = " + max);

        System.out.println("\n----------Task#3---------\n");
        int[] arr = {10, 5, 6, 7, 8, 5, 15, 15};

        findSecondGreatestAndSmallestWithSort(arr);

        System.out.println("\n----------Task#4---------\n");
        int[] arr3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallest(arr3);

        System.out.println("\n----------Task#5---------\n");
        String[] arr4 = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};

        findDuplicatedElementsInAnArray(arr4);

        System.out.println("\n----------Task#6---------\n");
        String[] arr5 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};

        findMostRepeatedElementInAnArray(arr5);
    }


    public static void findGreatestAndSmallestWithSort(int[] numbers) {
        Arrays.sort(numbers);
        System.out.println("Smallest = " + numbers[0]);
        System.out.println("Greatest = " + numbers[numbers.length - 1]);
    }

    public static void findSecondGreatestAndSmallestWithSort(int[] arr) {
        Arrays.sort(arr);

        arr = Arrays.stream(arr).distinct().toArray();

        Arrays.sort(arr);

        int secondSmallest = arr[1];
        int secondGreatest = arr[arr.length - 2];

        System.out.println("Second smallest = " + secondSmallest);
        System.out.println("Second greatest = " + secondGreatest);
    }

    public static void findSecondGreatestAndSmallest(int[] arr3) {

        int max = 10;
        int min = 10;
        int secondMax = 10;
        int secondMin = 10;

        for (int i = 0; i < arr3.length; i++) {
            if (arr3[i] > max) max = arr3[i];
            if (arr3[i] < min) min = arr3[i];

        }

        for (int i = 0; i < arr3.length; i++) {
            if (arr3[i] > min && arr3[i] < secondMin) secondMin = arr3[i];
            if (arr3[i] < max && arr3[i] > secondMax) secondMax = arr3[i];
        }
        System.out.println("Second Smallest = " + secondMin);
        System.out.println("Second Greatest = " + secondMax);
    }

    public static void findDuplicatedElementsInAnArray(String[] word) {
        for (int i = 0; i < word.length; i++) {
            for (int j = i + 1; j < word.length - 1; j++) {
                if (word[i].equals(word[j])) {
                    System.out.println(word[i]);
                    break;
                }
            }
        }
    }

    public static void findMostRepeatedElementInAnArray(String[] words) {
        String repeat = "";
        int counter = 0;
        int newCounter = 0;
        for (int i = 0; i < words.length - 1; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (words[i].equals(words[j])) {
                    newCounter++;
                    if (newCounter > counter) {
                        repeat = words[i];
                        counter = newCounter;
                    }
                }

            }
            newCounter = 0;

        }
        System.out.println(repeat);
    }
}
