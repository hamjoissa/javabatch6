package project;


public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n-------------------------TASK#1--------------------\n");

        String name = "Hamza";

        System.out.println("My name is = " + name);

        System.out.println("\n-------------------------TASK#2--------------------\n");

        char nameCharacter1 = 'H';
        char nameCharacter2 = 'a';
        char nameCharacter3 = 'm';
        char nameCharacter4 = 'z';
        char nameCharacter5 = 'a';

        System.out.println("Name letter 1 is = " + nameCharacter1);
        System.out.println("Name letter 2 is = " + nameCharacter2);
        System.out.println("Name letter 3 is = " + nameCharacter3);
        System.out.println("Name letter 4 is = " + nameCharacter4);
        System.out.println("Name letter 5 is = " + nameCharacter5);

        System.out.println("\n-------------------------TASK#3--------------------\n");

        String myFavMovie = "The Banker", myFavSong = "My House By: Flo Rida", myFavCity = "Chicago City", myFavActivity = "Playing Video Games", myFavSnack = "Oreo's";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite song is = " + myFavSong);
        System.out.println("My favorite city is = " + myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite snack is = " + myFavSnack);

        System.out.println("\n-------------------------TASK#4--------------------\n");

        int myFavNumber = 2, numberOfStatesIVisited = 10, numberOfCountriesIVisited = 4;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of states i visited is = " + numberOfStatesIVisited);
        System.out.println("The number of countries i visited is = " + numberOfCountriesIVisited);

        System.out.println("\n-------------------------TASK#5--------------------\n");

        boolean amIAtSchoolToday = false;

        System.out.println("Am I at school today = " + amIAtSchoolToday);

        System.out.println("\n-------------------------TASK#6--------------------\n");

        boolean isWeatherNiceToday = false;

        System.out.println("Weather is nice today = " + isWeatherNiceToday);







    }
}
