package project;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("\n-------------Task#1-------------\n");
        int[] numbers = {4, 8, 7, 15};
        System.out.println(findClosestDistance(numbers));

        System.out.println("\n-------------Task#2-------------\n");
        int[] numberss = {5, 3, -1, 3, 5, 7, -1};

        System.out.println(findSingleNumber(numberss));
        System.out.println("\n-------------Task#3-------------\n");
        String str = "Hello";
        System.out.println(findFirstUniqueCharacter(str));

        System.out.println("\n-------------Task#4-------------\n");
        int[] numbersss = {2, 3, 1, 5};
        System.out.println(findMissingNumber(numbersss));
    }
    //TASK01
    public static int findClosestDistance(int[] arr) {
        if (arr.length < 2) return -1;
        else {
            Arrays.sort(arr);
            int dist = Integer.MAX_VALUE;

            for (int i = 1; i < arr.length - 1; i++) {
                int currentDist = arr[i+1] - arr[i];
                dist = Math.min(currentDist, dist);
                if (dist > (arr[i + 1] - arr[i])) dist = arr[i + 1] - arr[i];

            }//END OF LOOP
            return dist;
        }//END OF ELSE


    }//END OF METHOD TASK 1


    //TASK02
    public static int findSingleNumber(int[] arr){
        //[5, 3, -1, 3, 5, 7, -1]
        int unique = 0;
        //Arrays.sort(arr);
        if (arr.length < 2) return arr[0];
        else {
            for (int i = 0; i < arr.length-1; i++) {
                if (arr[i] != arr[i+1]) unique = arr[i];
            }//END OF LOOP
        }//END OF ELSE
        return unique;
    }//END OF METHOD TASK2

    //TASK03
    public static char findFirstUniqueCharacter(String str){
        char unique = ' ';
        for (int i = 0; i < str.length(); i++) {
            if(str.indexOf(str.charAt(i), str.indexOf(str.charAt(i)) + 1) == -1){
                unique = str.charAt(i);
                break;
            }
        }
        return unique;
    }

    // TASK04
    public static int findMissingNumber(int[] arr){
        Arrays.sort(arr);
        int missingNo = 0;
        for(int i = 0; i < arr.length; i++ ){
            if(arr[i + 1] - arr[i] > 1 ){
                missingNo = arr[i] + 1;
                break;
            }
        }
        return missingNo;
    }

}
