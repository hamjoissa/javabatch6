package project;

import java.util.Random;
import java.util.Scanner;

public class Project03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random r = new Random();

        System.out.println("\n--------------------------TASK#1-----------------------------\n");

        String s1 = "24", s2 = "5";

        System.out.println("The sum of 24 and 5 = " + (Integer.parseInt(s1) + (Integer.parseInt(s2))));
        System.out.println("The subtraction of 24 and 5 = " + (Integer.parseInt(s1) - (Integer.parseInt(s2))));
        System.out.println("The division of 24 and 5 = " + (Integer.parseInt(s1) / (Integer.parseInt(s2))));
        System.out.println("The multiplication of 24 and 5 = " + (Integer.parseInt(s1) * (Integer.parseInt(s2))));
        System.out.println("The remainder of 24 and 5 = " + (Integer.parseInt(s1) % (Integer.parseInt(s2))));

        System.out.println("\n--------------------------TASK#2-----------------------------\n");
        int random = r.nextInt(35) + 1;

        System.out.println(random);

        System.out.println((random == 2) || (random == 3) || (random == 5) || (random == 7) || (random == 11) || (random == 13) ||
                (random == 17) || (random == 19) || (random == 23) || (random == 29) || (random == 31) ? random +
                " IS A PRIME NUMBER" : random + " IS NOT A PRIME NUMBER");

        System.out.println("\n--------------------------TASK#3-----------------------------\n");

        int random1 = r.nextInt(50) + 1;
        int random2 = r.nextInt(50) + 1;
        int random3 = r.nextInt(50) + 1;

        System.out.println("Random number 1 = " + random1);
        System.out.println("Random number 2 = " + random2);
        System.out.println("Random number 3 = " + random3);

        if (random1 > random2 && random2 > random3) {
            System.out.println("lowest number is = " + random3);
            System.out.println("Middle number is = " + random2);
            System.out.println("Greatest number is = " + random1);

        } else if (random2 > random1 && random1 > random3) {
            System.out.println("lowest number is = " + random3);
            System.out.println("Middle number is = " + random1);
            System.out.println("Greatest number is = " + random2);

        } else if (random3 > random2 && random2 > random1) {
            System.out.println("lowest number is = " + random1);
            System.out.println("Middle number is = " + random2);
            System.out.println("Greatest number is = " + random3);

        } else if (random1 > random3 && random3 > random2) {
            System.out.println("lowest number is = " + random2);
            System.out.println("Middle number is = " + random3);
            System.out.println("Greatest number is = " + random1);

        } else if (random2 > random3 && random3 > random1) {
            System.out.println("lowest number is = " + random1);
            System.out.println("Middle number is = " + random3);
            System.out.println("Greatest number is = " + random2);

        } else {
            System.out.println("lowest number is = " + random2);
            System.out.println("Middle number is = " + random1);
            System.out.println("Greatest number is = " + random3);
        }
        System.out.println("\n--------------------------TASK#4-----------------------------\n");

        char c1 = '5', c2 = 'a', c3 = 'R';


        if (c1 >= 65 && c1 <= 90)
        System.out.println("The letter is uppercase");

        else if (c1 >= 97 && c1 <= 122)
        System.out.println("The letter is lowercase");

        else
            System.out.println("Invalid character detected!!!");

        System.out.println("\n--------------------------TASK#5-----------------------------\n");

        char i1 = '#', i2 = 'e', i3 = 'R';

        if ((i1 == 65) || (i1 == 69) || (i1 == 73) || (i1 == 79) || (i1 == 85) || (i1 == 97) ||
                (i1 == 101) || (i1 == 105) || (i1 == 111) || (i1 == 117)) System.out.println("The letter is vowel");

        else if (i1 >=65 && i1 <= 90 || i1 >= 97 && i1 <= 122)  System.out.println("The letter is a consonant");

        else System.out.println("Invalid character detected!!!");

        System.out.println("\n--------------------------TASK#6-----------------------------\n");

        char c4 = '*';


        if (c4 >= 65 && c4 <= 90 || c4 >= 97 && c4 <= 122 || c4 >= 48 && c4 <= 57)
            System.out.println("Special character is = *");

        else
            System.out.println("Invalid character detected!!!");

        System.out.println("\n--------------------------TASK#7-----------------------------\n");


        char c5 = '*';


        if (c5 >= 48 && c5 <= 57)
            System.out.println("Character is digit");

        else if (c5 >= 65 && c5 <= 90 || c5 >= 97 && c5 <= 122)
            System.out.println("Character is a letter");

        else {
            System.out.println("Character is a special character");
            
        }


    }
}
