package project;

import utilities.ScannerHelper;

import java.util.Random;

public class Project05 {
    public static void main(String[] args) {
        System.out.println("\n----------------------TASK#1---------------------\n");

        String sentence = ScannerHelper.getString();

        int words = 1;

        for (int i = 0; i < sentence.length() - 1; i++) {
            if (sentence.charAt(i) == ' ' && sentence.charAt(i + 1) != ' ')
                words++;
        }


        if (words <= 1) {
            System.out.println("This sentence does not have multiple words");
        } else {
            // Print the word count
            System.out.println("The sentence has " + words + " words.");
        }
        System.out.println("\n----------------------TASK#2---------------------\n");

        Random r = new Random();

        String solution = "";

        int num1 = r.nextInt(26);
        int num2 = r.nextInt(26);

        int lower = Math.min(num1, num2);
        int upper = Math.max(num1, num2);

        // Print the range of numbers that are not divisible by 5
        System.out.println("Numbers between " + lower + " and " + upper + " that are not divisible by 5.");
        for (int i = lower; i <= upper; i++) {
            if (i % 5 != 0) {
                solution += i + " - ";
            }
        }
        System.out.println(solution.substring(0, solution.length() - 3));

        System.out.println("\n----------------------TASK#3---------------------\n");

        sentence = ScannerHelper.getString();

        int wordA = 0;

        for (int i = 0; i < sentence.length() - 1; i++) {

            char c = sentence.charAt(i);
            if (c == 'a' || c == 'A')
                wordA++;

        }
        if (sentence.isEmpty()) System.out.println("This sentence does not have any characters.");

        else System.out.println("This sentence has " + wordA + " a or A letters.");

        System.out.println("\n----------------------TASK#4---------------------\n");

        String str = ScannerHelper.getString();

        boolean isPalindrome = true;
        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                isPalindrome = false;
                break;
            }
        }

        // Print the result
        if (isPalindrome) {
            System.out.println(str + " is a palindrome.");
        } else {
            System.out.println(str + " is not a palindrome.");


        }
        System.out.println("\n----------------------TASK#5---------------------\n");

        int rows = 10;

        for (int i = 0; i < rows; i++) {
            // Print spaces before the stars
            for (int j = i; j < rows - 1; j++) {
                System.out.print(" ");
            }

            // Print the stars
            for (int j = 0; j < 2 * i + 1; j++) {
                System.out.print("*");
            }

            // Move to the next line
            System.out.println();
        }

    }
}
