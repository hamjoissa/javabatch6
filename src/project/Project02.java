package project;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n-----------------------TASK#1-------------------------\n");

        System.out.println("Please enter 3 numbers");
        int number = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();

        System.out.println("The product of the numbers entered is = " + (number * number2 * number3));

        System.out.println("\n-----------------------TASK#2-------------------------\n");

        System.out.println("What is your first name?");
        input.nextLine();
        String firstName = input.nextLine();

        System.out.println("What is your last name?");
        String lastName = input.nextLine();

        System.out.println("What is your year of birth?");
        int yearOfBirth = input.nextInt();

        int currentYear = 2023;

        System.out.println(firstName + " " + lastName + "'s age is = " + Math.abs(yearOfBirth - currentYear));

        System.out.println("\n-----------------------TASK#3-------------------------\n");

        System.out.println("What is your full name?");
        input.nextLine();
        String fullName = input.nextLine();

        System.out.println("What is your weight?");
        int weight = input.nextInt();

        System.out.println(fullName + "'s weight is = " + (weight * 2.205) + " lbs.");

        System.out.println("\n-----------------------TASK#4-------------------------\n");

        System.out.println("What is your full name?");
        input.nextLine();
        String fullName1 = input.nextLine();

        System.out.println("What is your age?");
        int age1 = input.nextInt();

        System.out.println("What is your full name?");
        input.nextLine();
        String fullName2 = input.nextLine();

        System.out.println("What is your age?");
        int age2 = input.nextInt();

        System.out.println("What is your full name?");
        input.nextLine();
        String fullName3 = input.nextLine();

        System.out.println("What is your age?");
        int age3 = input.nextInt();

        System.out.println(fullName1 + "'s age is " + age1 + ".");

        System.out.println(fullName2 + "'s age is " + age2 + ".");

        System.out.println(fullName3 + "'s age is " + age3 + ".");

        System.out.println("The average age is " + ((age1 + age2 + age3) / 3));

        System.out.println("The eldest age is " + Math.max(Math.max(age1, age2), age3) + ".");

        System.out.println("The youngest age is " + Math.min(Math.min(age1, age2), age3) + ".");


    }
}
