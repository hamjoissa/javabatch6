package methods;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class FindMaxMin {
    public static void main(String[] args) {

        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();
        int num3 = ScannerHelper.getNumber();

        System.out.println("The max = " + MathHelper.max(num1, num2, num3));
    }
}
