package methods;

import utilities.MathHelper;
import utilities.RandomGenerator;

public class Exercise01 {
    public static void main(String[] args) {

        int i1 = RandomGenerator.getRandomNumber(5,8);
        int i2 = RandomGenerator.getRandomNumber(3,4);
        int i3 = RandomGenerator.getRandomNumber(10,12);

        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);

        System.out.println("The max is = " + MathHelper.max(i1, i2, i3));
    }
}
