package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {

        // i can invoke static methods with class name
        // i can call non-static methods with object



        String name = ScannerHelper.getFirstName();

        System.out.println("The name entered by user = " + name);

        String lName = ScannerHelper.getLastName();

        System.out.println("The full name entered is = " + name + " " + lName);


        int age = ScannerHelper.getAge();

        System.out.println("");
    }
}
