package strings;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) that is used to store a sequence of characters
        TEXTS
         */

        String name ="John";
        String address = "chicago il 12345";

        System.out.println(name); // John
        System.out.println(address); // Chicago IL 12345


        System.out.println("\n---------------Task 1----------------\n");

        String favmovie = " The Banker";

        System.out.println("my favorite movie"+ favmovie);
    }
}
