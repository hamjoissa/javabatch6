package strings;

public class UnderstandingString {
    public static void main(String[] args) {
        String s1; // declaration of s1 as a String

        s1 = "TechGlobal School"; // intializing s1 as TechGlobal School

        String s2 = "is the best"; // declaration os s1 ad initialization of s2 as is the best;

        System.out.println("\n----------------CONCAT USING + ----------------\n");

        String s3 = s1 + " " + s2; // concatenation using plus sign
        System.out.println(s3); // TechGlobal School is the best

        System.out.println("\n---------------CONCAT USING METHOD--------------------\n");
        String s4 = s1.concat(" ").concat(s2);
        System.out.println(s4);

        String WordPart1 = "le";
        String WordPart2 = "ar";
        String WordPart3 = "ning";

        String FullWord = WordPart1 + WordPart2 + WordPart3;

        System.out.println(FullWord);


    }
}
