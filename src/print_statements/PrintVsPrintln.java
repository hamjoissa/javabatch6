package print_statements;

public class PrintVsPrintln {
    public static void main(String[] args) {
        // I will write code statements here

        //Println method
        System.out.println("Hello World");
        System.out.println("Today Is Sunday");
        System.out.println("John Doe");

        //print method
        System.out.println("Hello World");
        System.out.println("Today Is Sunday");
        System.out.println("John Doe");

    }
}
