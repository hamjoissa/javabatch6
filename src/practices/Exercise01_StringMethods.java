package practices;

import utilities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {

        System.out.println("\n------------TASK-1------------\n");

        String str = ScannerHelper.getString();

        System.out.println("The string given is = " + str);


        System.out.println("\n------------TASK-2------------\n");
        if(str.isEmpty()) System.out.println("The string given is empty");
        else System.out.println("The length is = " + str.length());

        System.out.println("\n------------TASK-3------------\n");

        if (str.isEmpty()) System.out.println("The first character = " + str.charAt(0));
        else System.out.println("There is no character in this String");

        System.out.println("\n------------TASK-4------------\n");

        if (!str.isEmpty()) System.out.println("The last character = " + str.charAt(str.length()-1));
        else System.out.println("There is no character in this String");

        System.out.println("\n------------TASK-5------------\n");

        if (str.contains("A") || str.contains("a") || str.contains("E") || str.contains("e") || str.contains("i") ||
                str.contains("I") || str.contains("u") || str.contains("U") || str.contains("o") || str.contains("O"))

            System.out.println("This String has vowel");
        else
            System.out.println("This String does not have vowel");




    }
}
