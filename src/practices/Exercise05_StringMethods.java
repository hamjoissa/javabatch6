package practices;

import utilities.ScannerHelper;

public class Exercise05_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        String str1 = ScannerHelper.getString();

        if (str.length() < 2 && str1.length() < 2) System.out.println("INVALID INPUT");
        else System.out.println(str.substring(1, str.length() -1) + str1.substring(1, str1.length() -1));
    }
}
