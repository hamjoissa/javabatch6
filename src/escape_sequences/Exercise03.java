package escape_sequences;

public class Exercise03 {
    public static void main(String[] args) {
        /*
        \n
        \t

        Monday\Tuesday\Wednesday
         */
        System.out.println("Monday\\Tuesday\\Wednesday");

        System.out.println("----------TASK-2------------");

        System.out.println("Good \\\\\\ morning");

    }
}
