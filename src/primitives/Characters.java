package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
        char -> 2 bytes
        itis used to store a single character
        letter, digit, space, specials

        datatype variablename = 'value';
         */

        char c1 = 'A';
        char c2 = ' ';

        System.out.println(c1); // A
        System.out.println(c2); //

        char myFavCharacter = 'P';

        System.out.println("my favorite char = " + myFavCharacter);
    }
}
