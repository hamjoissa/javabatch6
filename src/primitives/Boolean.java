package primitives;

public class Boolean {
    public static void main(String[] args) {
        /*
        boolean -> 1bit

        true
        false
         */

        boolean b1 = true;
        boolean b2 = false;
        boolean b3 = true;

        System.out.println(b1); // true
        System.out.println(b2); // false
        System.out.println(b3); // true

    }
}
