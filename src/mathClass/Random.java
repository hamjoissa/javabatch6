package mathClass;

public class Random {
    public static void main(String[] args) {
        System.out.println(Math.random()); // generates random number from 0.0 to 1.0

        System.out.println(Math.random() * 10); // generates random number from 0.0 to 10.0

        System.out.println(Math.random() * 100); // generates random number from 0.0 to 100.0

        System.out.println(Math.random() * 11); // generates random number from 0.0 to 11.0

        System.out.println(Math.round(Math.random() * 100));

        System.out.println(Math.round(Math.random() * 25));

        System.out.println("\n---------------------------Casting Sneak Peek-----------------------------\n");

        System.out.println((int) 25.5);


    }
}
