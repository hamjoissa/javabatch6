package mathClass;

import java.util.Scanner;

public class FindMin {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 2 numbers");

        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println("The min of the given numbers is = " + Math.min(num1, num2));

        System.out.println("\n------------------------Task#1----------------------\n");

        System.out.println("Please enter 3 numbers");

        int a1 = input.nextInt();
        int a2 = input.nextInt();
        int a3 = input.nextInt();

        System.out.println("The min of " + a1 + ", " + a2 + ", " + a3 + " is: " + (Math.min(Math.min(a1, a2), a3)));



    }
}
