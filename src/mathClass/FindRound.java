package mathClass;

public class FindRound {
    public static void main(String[] args) {
        System.out.println(Math.round(5.5)); // 6
        System.out.println(Math.round(45.3)); // 45
        System.out.println(Math.round(1.49999999)); // 1
    }
}
