package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int randomNumber = (int) Math.round(Math.random() * 50);


        System.out.println(randomNumber + " * 5 = " + (randomNumber * 5));

        int randomNum = (int) (Math.random() * 10 + 1);
        int randomNum2 = (int) (Math.random() * 10 + 1);

        int resultmin = Math.min(randomNum, randomNum2);
        int resultmax = Math.max(randomNum, randomNum2);

        System.out.println("Min number = " + resultmin);
        System.out.println("Max number = " + resultmax);

        System.out.println("Difference = " + Math.abs(randomNum - randomNum2));

        int randomNumber1 = (int) (Math.random() * 50 + 50);

        System.out.println(randomNumber1);

        System.out.println("The random number % 10 = " + (randomNumber1 % 10));

        System.out.println("Please enter five numbers from 1 to 10");
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();
        int number4 = input.nextInt();
        int number5 = input.nextInt();

        int ans1 = number1 * 5;
        int ans2 = number2 * 4;
        int ans3 = number3 * 3;
        int ans4 = number4 * 2;
        int ans5 = number5 * 1;

        int finalresult = ans1 + ans2 + ans3 + ans4 + ans5;

        System.out.println(finalresult);


    }
}
