package operators.arithmetic_operators;

public class Exercise02 {
    public static void main(String[] args) {

        double m1 = 90000, monthly = 12, weekly = 52, biweekly = 26;

        System.out.println("Monthly = $" + (m1 / monthly));
        System.out.println("Weekly = $" + (m1 / weekly));
        System.out.println("Bi-Weekly = $" + (m1 / biweekly));
    }
}
