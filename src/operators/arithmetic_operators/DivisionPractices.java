package operators.arithmetic_operators;

public class DivisionPractices {
    public static void main(String[] args) {
        /*
        5 / 2 = 2.5

        int 5 / int 2 = 2

        double 5 / double 2 = 2.5

         */

        int i1 = 5, i2 = 2;
        double d1 = 15, d2 = 2;

        int division1 = i1 / i2;
        double division2 = d1 / d2;

        System.out.println(division1);
        System.out.println(division2);

        System.out.println("The division of " + d1 + " by " + d2 + " = " + division2);

    }
}
