package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your Balance");
        double balance = input.nextDouble();

        System.out.println("The initial balance = $ " + balance);

        System.out.println("What is the first transaction amount?");
        double firsttrns = input.nextDouble();

        balance -= firsttrns;
        System.out.println("The balance after first transaction = $" + balance);

        System.out.println("What is is your second transaction");
        double secondtrns = input.nextDouble();

        balance -= secondtrns;

        System.out.println("Your balance after second transaction = $ " + balance);

        System.out.println("What is the third transaction amount?");
        double thirdtrns = input.nextDouble();

        balance -= thirdtrns;

        System.out.println("Your balance after third transaction = $ " + balance);


    }
}
