package Variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        String name = "John"; // declaring and initializing

        int age = 50; // declaring the variable without any value
        System.out.println(age);

        age = 50; // initializing the variable

        // capital letters and lowercase letters are seen differently with java

        double money = 5.5;
        double mOney = 4.5;
        double Money;

        double d1 = 10;
        System.out.println(d1);

        d1 = 15.5;
        System.out.println(d1);


    }
}
