package character_class;

import utilities.ScannerHelper;

public class CountingCharacters {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        int digits = 0;
        int letters = 0;

        for (int i = 0; i <= str.length()-1; i++) {
            if (Character.isDigit(str.charAt(i))) digits++;
            else if (Character.isLetter(str.charAt(i))) letters++;


        }
        System.out.println("This string has " + digits + " digits and " + letters + " letters");



        String str1 = ScannerHelper.getString();

        int upper = 0;
        int lower = 0;

        for (int i = 0; i <= str1.length()-1; i++) {
            if (Character.isUpperCase(str1.charAt(i))) upper++;
            else if (Character.isLowerCase(str1.charAt(i))) lower++;


        }
        System.out.println("This string has " + upper + " Upper cases and " + lower + " Lower cases");


        System.out.println("\n---------------TASK#4----------------\n");


        str = ScannerHelper.getString();

        upper = 0;
        lower = 0;
        letters = 0;
        digits = 0;
        int spaces = 0;
        int specials = 0;

        for (int i = 0; i <= str.length()-1; i++) {
            if (Character.isUpperCase(str.charAt(i))) upper++;
            else if (Character.isLowerCase(str.charAt(i))) lower++;
            else if (Character.isLetter(str.charAt(i))) letters++;
            else if (Character.isDigit(str.charAt(i))) digits++;
            else if (Character.isWhitespace(str.charAt(i))) spaces++;
            else if (!Character.isLetter(str.charAt(i)) && !Character.isDigit(str.charAt(i)) && !Character.isWhitespace(str.charAt(i))) specials++;


        }
        System.out.println("Uppercase letters = " + upper);
        System.out.println("Lowercase letters = " + lower);
        System.out.println("Letters = " + (upper + lower));
        System.out.println("Digits = " + digits);
        System.out.println("Spaces = " + spaces);
        System.out.println("Specials = " + specials);

    }
}
