package character_class;

import utilities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        char firstchar = str.charAt(0);

        System.out.println(65 <= firstchar && firstchar <= 90);

        System.out.println(Character.isUpperCase(firstchar));
        System.out.println(Character.isLetter(firstchar));
        System.out.println(Character.isLetterOrDigit(firstchar));
        System.out.println(Character.isLowerCase(firstchar));
        System.out.println(Character.isDigit(firstchar));
        System.out.println(Character.isWhitespace(firstchar));
        System.out.println(Character.isSpaceChar(firstchar));
    }
}
